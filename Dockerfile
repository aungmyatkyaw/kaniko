FROM registry.gitlab.com/aungmyatkyaw-cicd/base-images/alpine
RUN mkdir -p /kaniko && chmod 777 /kaniko

COPY --from=gcr.io/kaniko-project/executor:debug /kaniko/ /kaniko/
COPY --from=gcr.io/kaniko-project/executor:debug /etc/nsswitch.conf /etc/nsswitch.conf

ENV HOME /root
ENV USER root
ENV PATH $PATH:/usr/local/bin:/kaniko
ENV SSL_CERT_DIR=/kaniko/ssl/certs
ENV DOCKER_CONFIG /kaniko/.docker/
ENV DOCKER_CREDENTIAL_GCR_CONFIG /kaniko/.config/gcloud/docker_credential_gcr_config.json
WORKDIR /workspace

ENTRYPOINT ["/kaniko/executor"]
